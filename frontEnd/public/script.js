$(document).ready(function() {
    const apiUrl = 'http://localhost:3001/api';
    $.ajax({
        url: apiUrl
    }).then(function(data) {
        $('#dataFromAPI').text(data.message);
    }).catch(function (error) {
        $('#dataFromAPI').text(JSON.stringify(error));
    });
});